# Change Log

## 0.2.0 - 2018-07-24
### Changed
- Alter API for table-fn key to take proper keys for react rendering

## 0.1.0 - 2016-07-01
### Added
- Lazy table rendering (basic feature). Initial commit and all the usual jazz